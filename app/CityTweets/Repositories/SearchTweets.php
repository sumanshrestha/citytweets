<?php
namespace App\CityTweets\Repositories;

use Cache;
use Carbon\Carbon;
use Geocoder;
use Twitter;

class SearchTweets
{
    /**
     * Store city name
     * @var string
     */
    public $city;

    /**
     * Store coordinates
     * @var object
     */
    public $coordinates;

    /**
     * Configure search redius
     * @var string
     */
    public $search_radius = '50km';

    /**
     * Configure cache time (in minutes)
     * @var int
     */
    public $cache_time = 60;

    /**
     * Store response
     * @var array
     */
    public $response = [];

    /**
     * Store processed tweets
     * @var array
     */
    public $list = [];

    /**
     * Set coordinates of the city
     * @return SearchTweets
     */
    public function setCoordinates()
    {
        $geocode = Geocoder::geocode($this->city);
        if (!empty($geocode->getCity())) {
            $this->coordinates = $geocode;
        }
    }

    /**
     * Search tweets
     * @return SearchTweets
     */
    public function searchTweets()
    {
        if (!empty($this->coordinates)) {
            $geocode = [$this->coordinates->getLatitude(), $this->coordinates->getLongitude(), $this->search_radius];
            $this->response = Twitter::getSearch([
                'q' => $this->city,
                'geocode' => $geocode,
                'count' => '20'
            ]);
        }
    }

    /**
     * Get Tweets
     * @return SearchTweets
     */
    public function getTweets()
    {
        // cache tweets per city
        Cache::remember($this->city, $this->cache_time, function () {
            $this->setCoordinates();
            $this->searchTweets();
        });
    }

    /**
     * List Tweets
     * @param $city
     * @return array $search
     */
    public function listTweets($city)
    {
        $this->city = $city;
        $this->getTweets();

        // get list of status with coordinates
        if (!empty($this->response)) {
            foreach ($this->response->statuses as $status) {

                // only keep statuses with coordinates
                if (!empty($status->coordinates)) {
                    array_push($this->list, [
                        'photo' => $status->user->profile_image_url,
                        'lat' => $status->coordinates->coordinates[0],
                        'long' => $status->coordinates->coordinates[1],
                        'status' => $status->text,
                        'datetime' => Carbon::parse($status->created_at)->format('Y-m-d h:i:s')
                    ]);
                }
            }

            $search = [
                'list' => json_encode($this->list),
                'coordinates' => $this->coordinates,
                'city' => $this->city
            ];
        } else {
            $search = ['error' => 'No response from twitter'];
        }
        if (empty($this->list)) {
            $search = [
                'city' => $this->city,
                'coordinates' => $this->coordinates,
                'error' => 'No tweets found'
            ];
        }

        return $search;
    }
}
