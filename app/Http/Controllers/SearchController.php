<?php

namespace App\Http\Controllers;

use App\CityTweets\Repositories\SearchTweets;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    /**
     * Show search page
     * @return mixed
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Search tweets
     * @param SearchTweets $search
     * @param Request $request
     * @return mixed
     */
    public function search(SearchTweets $search, Request $request)
    {
        $city = $request->input('city');

        if (empty($city)) {

            return view('home');
        } else {
            $search = $search->listTweets($city);

            return view('home', $search);
        }
    }
}
