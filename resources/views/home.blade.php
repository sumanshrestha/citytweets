<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>CityTweets</title>

    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

<div id="search">
    <form action="search" method="get">
        <div class="form-group">
            <input type="text" name="city" class="text-field" placeholder="Enter a city name"
                   value="{{ isset($city) ? $city : ''}}">
        </div>
        <div class="form-group">
            <button type="submit" class="button">Search</button>
            <button type="button" class="button">History</button>
        </div>
    </form>
</div>

<div id="map">
    <div class="title">
        @if (isset($error))
            {{ $error }}
        @else
            @if(!empty($city))
                Tweets about {{ $city }}
            @else
                Search for a city
            @endif
        @endif
    </div>
    <div id="map-canvas"></div>
</div>

@if(!empty($city))
    {{-- Google Map --}}
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script src="assets/js/richmarker-compiled.js"></script>
    <script type="text/javascript">
        function initialize() {
            var mapOptions = {
                center: new google.maps.LatLng('{{ $coordinates->getLatitude() }}', '{{ $coordinates->getLongitude() }}'),
                zoom: 12,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                zoomControl: true,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                }
            };
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            var infowindow = new google.maps.InfoWindow();
            var locations = {!! isset($list) ? $list : '""'!!};
            for (i = 0; i < locations.length; i++) {
                var position = new google.maps.LatLng(locations[i]['long'], locations[i]['lat']);
                var marker = new RichMarker({
                    position: position,
                    map: map,
                    flat: true,
                    anchor: RichMarkerPosition.MIDDLE,
                    content: '<div class="marker"><img src="' + locations[i]['photo'] + '"></div>'
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent('<strong>Tweet:</strong> ' + locations[i]['status'] + '<br><strong>When:</strong> ' + locations[i]['datetime']);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endif

</body>
</html>